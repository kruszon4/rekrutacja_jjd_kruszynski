import com.google.gson.JsonSyntaxException;
import pl.rekrutacja.nbp.Controller.Controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.Scanner;

//Klasa główna - tworzy obiekt klasy Controller - i wywołuje metody niezbędne do zrezalizowania zadania
public class Main {
    public static void main(String[] args) throws IOException, ParseException {

        String quit = "";
        Scanner scanner = new Scanner(System.in);


        do {

            try {
                System.out.println("Wybierz walutę");
                System.out.println();
                Controller controller = new Controller();

                controller.effiectiveDateList();
                System.out.print("Odchylenie standardowe waluty w powyższym zakresie dat : ");
                System.out.format("%.4f%n", controller.standardDeviation());
                System.out.print("Średnia kupna walut w powyższym zakresie dat : ");
                System.out.format("%.4f%n", controller.average());

                System.out.println("------------------------------------------------------------");
                System.out.println("Wciśnij enter aby kontynuować bądź wpisz quit aby zakończyć");
                quit = scanner.nextLine().toLowerCase();
            } catch (UnknownHostException eu) {
                System.err.println("Brak połączenia z internetem");
                break;

            } catch (FileNotFoundException fe) {
                System.err.println("Nie znaleziono danych dla podanego zakresu");
                break;
            } catch (JsonSyntaxException | IllegalStateException je) {
                System.err.println("Błąd połączenia z API włącz aplikację ponownie");
                break;
            }
        }

        while (!quit.equals("quit"));


    }
}
