package pl.rekrutacja.nbp.Gson;

import java.util.ArrayList;
import java.util.List;

//Klasa tworząca obiekt z JSON

public class GsonNbp {
    private List<Rates> rates = new ArrayList<Rates>();
    private String code;
    private String table;
    private String currency;


    public List<Rates> getRates() {
        return rates;
    }


    public String getCode() {
        return code;
    }


    public String getTable() {
        return table;
    }


    public String getCurrency() {
        return currency;
    }


    @Override
    public String toString() {
        return "ClassPojo [rates = " + rates + ", code = " + code + ", table = " + table + ", currency = " + currency + "]";
    }
}
