package pl.rekrutacja.nbp.Gson;

//Klasa tworząca obiekt z JSON
public class Rates {
    private String no;
    private String ask;
    private String code;
    private String effectiveDate;
    private String bid;
    private String currency;


    public String getCode() {
        return code;
    }

    public String getCurrency() {
        return currency;
    }

    public String getNo() {
        return no;
    }


    public String getAsk() {
        return ask;
    }


    public String getEffectiveDate() {
        return effectiveDate;
    }


    public String getBid() {
        return bid;
    }


    @Override
    public String toString() {
        return "ClassPojo [no = " + no + ", ask = " + ask + ", effectiveDate = " + effectiveDate + ", bid = " + bid + "]";
    }
}

