package pl.rekrutacja.nbp.Controller;

import com.google.gson.Gson;
import pl.rekrutacja.nbp.Gson.GsonNbp;
import pl.rekrutacja.nbp.Url.UrlText;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//Klasa pobiera listę walut - świadomie łączy się osobno z API NBP aby w późnieszym czasie(klasa Controller) pobierać dane tylko
// dla konkretnej waluty zamiast dużego pliku z wszystkimi walutami dla zadanego okresu czasu (plik jest zauważalnie lżejszy)

public class Display {

    private UrlText urlText = new UrlText();
    private Gson gson = new Gson();
    private List<String> currencyList = new ArrayList<>();


    public List<String> getCurrencyList() {
        return currencyList;
    }

    public void currencyCodeList() throws IOException {

        String jsonText = urlText.getTextFromUrl("http://api.nbp.pl/api/exchangerates/tables/c");
        GsonNbp[] gsonNbp2 = gson.fromJson(jsonText, GsonNbp[].class);


        for (int i = 0; i < gsonNbp2[0].getRates().size(); i++) {
            String currency = gsonNbp2[0].getRates().get(i).getCurrency();
            String currencyCode = gsonNbp2[0].getRates().get(i).getCode();
            this.currencyList.add(currencyCode);
            System.out.println(i + 1 + " " + currency.toUpperCase());

        }

    }
}
