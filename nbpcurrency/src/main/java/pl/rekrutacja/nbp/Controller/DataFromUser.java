package pl.rekrutacja.nbp.Controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//Klasa z metodami odpowiedzalnymi za pobieranie danych od użytkownika w odpowiednim formacie -
//zaimplementowano zabezpieczenia przed podawaniem daty w niepoprawnym formacie i konfiguracji

public class DataFromUser {
    Scanner scanner = new Scanner(System.in);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");


    public String StringFromUser() throws IOException, ParseException {

        Display display = new Display();
        display.currencyCodeList();
        String numberOfCurrency = scanner.nextLine();
        Pattern pattern = Pattern.compile("(\\d{1,})");

        Matcher matcher = pattern.matcher(numberOfCurrency);
        boolean matches = matcher.matches();


        while (!matches || (Integer.parseInt(numberOfCurrency) < 1 ||
                Integer.parseInt(numberOfCurrency) > display.getCurrencyList().size())) {
            System.out.println("Musisz podać liczbę z listy");
            numberOfCurrency = scanner.nextLine();
            matcher = pattern.matcher(numberOfCurrency);
            matches = matcher.matches();
        }

        String currency = display.getCurrencyList().get(Integer.parseInt(numberOfCurrency) - 1);

        String startDate = "";
        String endDate = "";
        Date dateStart;
        boolean dateBoolean;
        Date dateEnd;
        do {
            System.out.println("Podaj datę początkową w formacie YYYY-MM-DD");
            startDate = dataCompiler();
            System.out.println("Podaj datę końcową w formacie YYYY-MM-DD");
            endDate = dataCompiler();

            dateStart = simpleDateFormat.parse(startDate);
            dateEnd = simpleDateFormat.parse(endDate);

            if (dateBoolean = dateStart.after(dateEnd)) {
                System.out.println("Data końcowa musi być po dacie początkowej");
            }


            long differentTime = Math.abs(dateEnd.getTime() - dateStart.getTime());
            long diff = TimeUnit.DAYS.convert(differentTime, TimeUnit.MILLISECONDS);

            if (diff > 93) {

                System.out.println("Maksymalny przedzial czasu to 92 dni");
                dateBoolean = true;
            }

        } while (dateBoolean);


        String mainUrl = "http://api.nbp.pl/api/exchangerates/rates/c/";

        return new StringBuilder().append(mainUrl)
                .append(currency).append("/").append(startDate).append("/").append(endDate).toString();
    }


    public String dataCompiler() throws ParseException {

        Pattern pattern = Pattern.compile("([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))");


        String str = scanner.nextLine();

        Matcher matcher = pattern.matcher(str);
        boolean matches = matcher.matches();


        while (!matcher.matches()) {

            System.out.println("Podaj poprawną datę w formacie YYYY-MM-DD");
            str = scanner.nextLine();
            matcher = pattern.matcher(str);
        }


        Date nowDate = new Date();
        Date userDate = simpleDateFormat.parse(str);


        if (userDate.after(nowDate)) {

            System.out.println("Takiego dnia jeszcze nie było. Podaj ponownie datę:");
            str = dataCompiler();

        }


        return str;
    }


}