package pl.rekrutacja.nbp.Controller;

import com.google.gson.Gson;
import pl.rekrutacja.nbp.Gson.GsonNbp;
import pl.rekrutacja.nbp.Gson.Rates;
import pl.rekrutacja.nbp.Url.UrlText;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


//Głowna klasa odpowiadająca za komunikację z serwerem NBP
// oraz konwersję JSON na obiekt(do konwersji korzystam z zewnętrznej biblioteki GSON)
//oraz obliczenie parametrów z zadania (średnia i odchylenie strandardowe)

public class Controller {
    private Gson gson = new Gson();
    private List<Rates> rates;
    private GsonNbp gsonNbp;
    UrlText urlText = new UrlText();


    public Controller() throws IOException, ParseException {

        String textUrl = new DataFromUser().StringFromUser();

        System.out.println("---------------------Pobieram dane z serwera NBP---------------------");
        String jsonText = urlText.getTextFromUrl(textUrl);
        this.gsonNbp = gson.fromJson(jsonText, GsonNbp.class);
        this.rates = gsonNbp.getRates();

    }


    private List<Double> gsonAsk() throws IOException {

        List<Double> doubleList = new ArrayList<Double>();


        for (Rates rate : rates) {
            doubleList.add(Double.parseDouble(rate.getAsk()));
        }


        return doubleList;
    }


    public void effiectiveDateList() throws IOException {

        List<String> date = new ArrayList<String>();
        for (Rates rs : rates
                ) {

            date.add(rates.get(rates.indexOf(rs)).getEffectiveDate());

        }

        System.out.println("Pobrano dane dla waluty : " + gsonNbp.getCurrency().toUpperCase() + " z zakresu dat " + date.get(0) + " - " + date.get(date.size() - 1));


    }


    private List<Double> gsonBid() throws IOException {

        List<Double> doubleList = new ArrayList<Double>();


        for (Rates rate : rates) {
            doubleList.add(Double.parseDouble(rate.getBid()));
        }


        return doubleList;
    }

    public double average() throws IOException {
        List<Double> list = new ArrayList<>(gsonBid());

        double sum = 0;

        for (Double doubles : list
                ) {
            sum += doubles;
        }


        return (sum / list.size());
    }

    public double standardDeviation() throws IOException {
        List<Double> list = new ArrayList<>(gsonAsk());
        double tmp = 0;
        double avg = 0;
        for (Double doubles : list
                ) {
            avg += doubles;
        }
        avg = avg / list.size();

        for (int i = 0; i < list.size(); i++) {

            tmp += Math.pow((list.get(i) - avg), 2);
        }

        return Math.sqrt(tmp / list.size());
    }

}
