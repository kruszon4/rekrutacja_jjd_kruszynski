package pl.rekrutacja.nbp.Url;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

//Klasa z motodą odpowiedzialną za poprawne połaczenie z URL i budowę JSON
public class UrlText {

    public String getTextFromUrl(String urlText) throws IOException {
        URL url = new URL(urlText);

        URLConnection connection = url.openConnection();
        connection.addRequestProperty("User-Agent", "Chrome");
        InputStream inputStream = connection.getInputStream();

        Scanner scanner = new Scanner(inputStream);
        StringBuilder sb = new StringBuilder();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            sb.append(line).append("\n");
        }

        return sb.toString();
    }


}
